#include <stdio.h>
struct employee
{
        char name[20];
        int ID;
        float salary;
        char DOJ[10];
};
int main()
{
    struct employee emp;
    printf("Enter name of the employee\n");
    scanf("%s",&emp.name);
    printf("Enter the ID of the employee\n");
    scanf("%d",&emp.ID);
    printf("Enter the salary of the employee\n");
    scanf("%f",&emp.salary);
    printf("Enter the date of joining of the employee\n");
    scanf("%s",&emp.DOJ);
    printf("The details entered is:\n");
    printf("Name of the employee is %s\n",emp.name);
    printf("ID of the employee is %d\n",emp.ID);
    printf("Salary of the employee is %f\n",emp.salary);
    printf("Date of joining of the employee is %s\n",emp.DOJ);
    return 0;
}
