#include <stdio.h>
void swap(int*a,int*b);

int main()
{
    int a,b;
    printf("Enter a and b\n");
    scanf("%d%d",&a,&b);
    printf("Numbers before swapping are a=%d and b=%d\n",a,b);
    swap(&a,&b);
    printf("Swapped numbers are a=%d and b=%d",a,b);
    return 0;
}
void swap(int*x,int*y)
{
    int t;
    t=*x;
    *x=*y;
    *y=t;
}
